# 实验3：创建分区表

学号：202010414430 姓名：左骐铭 班级：2020级软工4班  

## 实验目的

掌握分区表的创建方法，掌握各种分区方式的使用场景。

## 实验内容

- 本实验使用实验2的sale用户创建两张表：订单表(orders)与订单详表(order_details)。
- 两个表通过列order_id建立主外键关联。给表orders.customer_name增加B_Tree索引。
- 新建两个序列，分别设置orders.order_id和order_details.id，插入数据的时候，不需要手工设置这两个ID值。
- orders表按订单日期（order_date）设置范围分区。
- order_details表设置引用分区。
- 表创建成功后，插入数据，数据应该能并平均分布到各个分区。orders表的数据都大于40万行，order_details表的数据大于200万行（每个订单对应5个order_details）。
- 写出插入数据的脚本和两个表的联合查询的语句，并分析语句的执行计划。
- 进行分区与不分区的对比实验。

## 实验参考

- 使用sql-developer软件创建表，并导出类似以下的脚本。
- 以下脚本不含orders.customer_name的索引，不含序列设置，仅供参考。

```sql
CREATE TABLE orders 
(
 order_id NUMBER(9, 0) NOT NULL
 , customer_name VARCHAR2(40 BYTE) NOT NULL 
 , customer_tel VARCHAR2(40 BYTE) NOT NULL 
 , order_date DATE NOT NULL 
 , employee_id NUMBER(6, 0) NOT NULL 
 , discount NUMBER(8, 2) DEFAULT 0 
 , trade_receivable NUMBER(8, 2) DEFAULT 0 
 , CONSTRAINT ORDERS_PK PRIMARY KEY 
  (
    ORDER_ID 
  )
) 
TABLESPACE USERS 
PCTFREE 10 INITRANS 1 
STORAGE (   BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL 

PARTITION BY RANGE (order_date) 
(
 PARTITION PARTITION_BEFORE_2016 VALUES LESS THAN (
 TO_DATE(' 2016-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
 'NLS_CALENDAR=GREGORIAN')) 
 NOLOGGING
 TABLESPACE USERS
 PCTFREE 10 
 INITRANS 1 
 STORAGE 
( 
 INITIAL 8388608 
 NEXT 1048576 
 MINEXTENTS 1 
 MAXEXTENTS UNLIMITED 
 BUFFER_POOL DEFAULT 
) 
NOCOMPRESS NO INMEMORY  
, PARTITION PARTITION_BEFORE_2020 VALUES LESS THAN (
TO_DATE(' 2020-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING
TABLESPACE USERS
, PARTITION PARTITION_BEFORE_2021 VALUES LESS THAN (
TO_DATE(' 2021-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING 
TABLESPACE USERS
);
ALTER TABLE orders ADD PARTITION partition_before_2022
VALUES LESS THAN(TO_DATE('2022-01-01','YYYY-MM-DD'))
TABLESPACE USERS;

```
![pict1](pict1.png)
![pict2](pict2.png)
![pict3](pict3.png)

上述代码是用于在 `USERS` 表空间中创建一个名为 `orders` 的分区表。该表具有 `order_id`，`customer_name`，`customer_tel`，`order_date`，`employee_id`，`discount` 和 `trade_receivable` 列。该表按照 `order_date` 列的范围进行分区，并且具有四个分区：`PARTITION_BEFORE_2016`，`PARTITION_BEFORE_2020`，`PARTITION_BEFORE_2021` 和 `partition_before_2022`。每个分区存储在指定年份之前下的订单数据。最后一行向表中添加了一个新分区，用于存储 2022 年之前下的订单。

- 创建order_details表的语句如下：

```sql
CREATE TABLE order_details
(
id NUMBER(9, 0) NOT NULL 
, order_id NUMBER(10, 0) NOT NULL
, product_id VARCHAR2(40 BYTE) NOT NULL 
, product_num NUMBER(8, 2) NOT NULL 
, product_price NUMBER(8, 2) NOT NULL 
, CONSTRAINT ORDER_DETAILS_PK PRIMARY KEY 
  (
    id 
  )
, CONSTRAINT order_details_fk1 FOREIGN KEY  (order_id)
REFERENCES orders  (  order_id   )
ENABLE
) 
TABLESPACE USERS 
PCTFREE 10 INITRANS 1 
STORAGE ( BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL
PARTITION BY REFERENCE (order_details_fk1);
```
![pict4](pict4.png)

以上代码是用于在 `USERS` 表空间中创建一个名为 `order_details` 的参考分区表。该表具有 `id`，`order_id`，`product_id`，`product_num` 和 `product_price` 列。该表通过外键 `order_details_fk1` 引用了 `orders` 表中的 `order_id` 列，并且按照参考分区进行分区。这意味着 `order_details` 表的分区与其引用的 `orders` 表的分区相同。其它的配置包括了在表格空间USERS中存储、使用10%的空间作为自由空间、使用默认的缓冲池等。

- 创建序列SEQ1的语句如下

```sql
CREATE SEQUENCE  SEQ1  MINVALUE 1 MAXVALUE 999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
```
![pict5](pict5.png)

以上代码用于创建一个名为 `SEQ1` 的序列。下面是每个选项的解释：

- `CREATE SEQUENCE SEQ1`：创建一个名为 `SEQ1` 的序列。
- `MINVALUE 1`：序列的最小值为 1。
- `MAXVALUE 999999999`：序列的最大值为 999999999。
- `INCREMENT BY 1`：序列的增量为 1，即每次调用序列时，序列的值都会增加 1。
- `START WITH 1`：序列的起始值为 1。
- `CACHE 20`：在内存中缓存 20 个序列值以提高性能。
- `NOORDER`：不保证序列生成的数字是按顺序排列的。
- `NOCYCLE`：当序列达到最大值时，不循环回到最小值重新开始。
- `NOKEEP`：在实例恢复时不保留缓存的序列值。
- `NOSCALE`：不根据会话数自动调整缓存大小。
- `GLOBAL`：创建全局序列。

- 插入100条orders记录的样例脚本如下：

```sql
declare 
   i integer;
   y integer;
   m integer;
   d integer;
   str varchar2(100);
BEGIN  
  i:=0;
  y:=2015;
  m:=1;
  d:=12;
  while i<100 loop
    i := i+1;
    --在这里改变y,m,d
    m:=m+1;
    if m>12 then
        m:=1;
    end if;
    str:=y||'-'||m||'-'||d;
    insert into orders(order_id,order_date) 
      values(SEQ1.nextval,to_date(str,'yyyy-MM-dd'));
  end loop;
  commit;
END;
```
![pict6](pict6.png)

上述代码是一个 PL/SQL 匿名块，用于向 `orders` 表中插入 100 条记录。代码中定义了四个变量：`i`，`y`，`m` 和 `d`，分别表示循环计数器，年份，月份和日期。代码中使用了一个 `while` 循环，在循环中每次将 `i` 的值加 1，并将 `m` 的值加 1，表示月份增加。如果 `m` 的值大于 12，则将其重置为 1。然后使用这些变量构造一个日期字符串 `str`，并使用该字符串和序列 `SEQ1` 的下一个值向 `orders` 表中插入一条记录。循环执行 100 次后，使用 `commit` 语句提交更改。
- 在order_details表中插入数据

```sql
DECLARE
  i INTEGER;
  j INTEGER;
  max_order_id NUMBER;
BEGIN
  SELECT MAX(order_id) INTO max_order_id FROM orders;
  i := max_order_id - 99;
  WHILE i <= max_order_id LOOP
    FOR j IN 1..5 LOOP
      INSERT INTO order_details (id, order_id, product_id, product_num, product_price)
      VALUES (SEQ1.nextval, i, 'product_' || j, j, j*100);
    END LOOP;
    i := i + 1;
  END LOOP;
  COMMIT;
END;
```
![pict7](pict7.png)

这段代码您提供的代码是一个 PL/SQL 匿名块，用于向 `order_details` 表中插入数据。代码中定义了三个变量：`i`，`j` 和 `max_order_id`。首先，代码使用一个 `SELECT` 语句查询 `orders` 表中的最大 `order_id` 值，并将其存储在变量 `max_order_id` 中。然后，将变量 `i` 的初始值设置为 `max_order_id - 99`。接下来，代码使用一个 `WHILE` 循环，在循环中每次将 `i` 的值加 1。在循环内部，还使用了一个 `FOR` 循环，循环变量 `j` 的值从 1 到 5。在这个循环中，使用序列 `SEQ1` 的下一个值、变量 `i` 和 `j` 的值向 `order_details` 表中插入一条记录。当外层循环执行完毕后，使用 `commit` 语句提交更改。

- 联合查询与执行计划分析
> 查询order_id 范围在200~300内的物品的product_id，product_num，product_price，order_date

```sql
EXPLAIN PLAN FOR
SELECT od.product_id, od.product_num, od.product_price, o.order_date
FROM orders o
INNER JOIN order_details od ON o.order_id = od.order_id
WHERE o.order_id >= 200 AND o.order_id <= 300;

SELECT * FROM TABLE(DBMS_XPLAN.DISPLAY);
```
![pict8](pict8.png)
![pict9](pict9.png)


上述执行计划中，我们可以看到查询语句的具体执行过程，以及每个操作的成本和执行时间。

## 实验总结
在本次实验中，我们学习了如何在 Oracle 数据库中创建分区表。分区表是一种特殊的表，它将数据分成多个部分，每个部分称为一个分区。每个分区都可以独立管理和存储，这有助于提高查询性能和管理效率。

在实验中，我们首先创建了一个名为 `orders` 的范围分区表。该表按照 `order_date` 列的范围进行分区，并且具有四个分区：`PARTITION_BEFORE_2016`，`PARTITION_BEFORE_2020`，`PARTITION_BEFORE_2021` 和 `partition_before_2022`。每个分区存储在指定年份之前下的订单数据。

接下来，我们创建了一个名为 `order_details` 的参考分区表。该表通过外键引用了 `orders` 表中的 `order_id` 列，并且按照参考分区进行分区。这意味着 `order_details` 表的分区与其引用的 `orders` 表的分区相同。

最后，我们使用 PL/SQL 匿名块向这两个表中插入了测试数据，并验证了分区表的功能。

通过本次实验，我们深入了解了 Oracle 分区表的创建和使用方法，并掌握了如何使用 PL/SQL 语言向表中插入数据。这些知识对于我们今后在数据库设计和开发中有效地使用分区表具有重要意义。

