﻿# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计
  
姓名：左骐铭    班级：软件工程4班    学号：202010414430

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|

## 步骤
### 1、创建表空间

```
-- 创建索引表空间
CREATE TABLESPACE other_tablespace
DATAFILE 'other_tablespace.dbf'
SIZE 100M
AUTOEXTEND ON
EXTENT MANAGEMENT LOCAL
SEGMENT SPACE MANAGEMENT AUTO;
```
![表空间](pic1.png)

### 2、创建表

#### 1、用户表

```
-- 创建用户表
CREATE TABLE "User" (
    user_id NUMBER PRIMARY KEY,
    username VARCHAR2(50),
    password VARCHAR2(50),
    privilege VARCHAR2(50),
    email VARCHAR2(50)
)TABLESPACE other_tablespace;
```
![用户表](pic5.png)

#### 2、订单表

```
-- 创建订单表
CREATE TABLE "Order" (
    order_id NUMBER PRIMARY KEY,
    customer_id NUMBER,
    order_date DATE,
    status VARCHAR2(50)
)TABLESPACE other_tablespace;
```
![订单表](pic3.png)

#### 3、订单明细表

```
-- 创建订单明细表
CREATE TABLE OrderDetail (
    order_detail_id NUMBER PRIMARY KEY,
    order_id NUMBER,
    product_id NUMBER,
    quantity NUMBER,
    unit_price NUMBER,
    FOREIGN KEY (order_id) REFERENCES "Order"(order_id),
    FOREIGN KEY (product_id) REFERENCES Product(product_id)
)TABLESPACE other_tablespace;
```
![订单明细表](pic4.png)

#### 4、商品表

```
CREATE TABLE Product (
    product_id NUMBER PRIMARY KEY,
    name VARCHAR2(100),
    price NUMBER,
    stock NUMBER
)TABLESPACE other_tablespace;
```
![商品表](pic2.png)

### 3、模拟数据

#### 用户表

```
--为用户表插入十万模拟数据
BEGIN
    FOR i IN 1..100000 LOOP
        -- 生成权限值（1或2）
        DECLARE
            v_privilege NUMBER := CASE WHEN MOD(i, 2) = 0 THEN 2 ELSE 1 END;
        BEGIN
            INSERT INTO "User" (user_id, username, password, privilege, email)
            VALUES (i, 'User' || i, 'password' || i, v_privilege, 'user' || i || '@example.com');
            COMMIT;
        END;
    END LOOP;
END;
```
![用户数据](pic15.png)

#### 订单表

```
--为订单表插入十万模拟数据
BEGIN
    FOR i IN 1..100000 LOOP
        INSERT INTO "order" (order_id, customer_id, order_date, status)
        VALUES (i, ROUND(DBMS_RANDOM.VALUE(1, 100), 0), SYSDATE - ROUND(DBMS_RANDOM.VALUE(1, 365), 0), 'Completed');
        COMMIT;
    END LOOP;
END;
```
![订单数据](pic18.png)

#### 订单明细表

```
--为订单明细表插入十万模拟数据
BEGIN
    FOR i IN 1..100000 LOOP
        INSERT INTO OrderDetail (order_detail_id, order_id, product_id, quantity, unit_price)
        VALUES (i, i, ROUND(DBMS_RANDOM.VALUE(1, 100000), 0), ROUND(DBMS_RANDOM.VALUE(1, 10), 0), ROUND(DBMS_RANDOM.VALUE(10, 100), 2));
        COMMIT;
    END LOOP;
END;
```
![订单数据](pic17.png)

#### 商品表

```
--为商品表插入十万模拟数据
BEGIN
    FOR i IN 1..100000 LOOP
        INSERT INTO Product (product_id, name, price, stock)
        VALUES (i, 'Product ' || i, ROUND(DBMS_RANDOM.VALUE(10, 100), 2), ROUND(DBMS_RANDOM.VALUE(0, 1000)));
        COMMIT;
    END LOOP;
END;
```
![订单数据](pic16.png)

### 4、用户管理

```
-- 创建admin用户并授予完全权限
CREATE USER admin IDENTIFIED BY admin_password;
GRANT ALL PRIVILEGES TO admin;

-- 创建customer用户并授予部分权限
CREATE USER customer IDENTIFIED BY customer_password;
GRANT SELECT, INSERT, UPDATE, DELETE ON Product TO customer;
GRANT SELECT, INSERT, UPDATE, DELETE ON "order" TO customer;
GRANT SELECT, INSERT, UPDATE, DELETE ON "user" TO customer;
```
![用户权限](pic6.png)  
![用户权限](pic7.png)  
![用户权限](pic8.png)

### 5、PL/SQL设计

#### 1、创建程序包
```
-- 创建程序包
CREATE OR REPLACE PACKAGE SalesPackage AS
    -- 存储过程：生成订单
    PROCEDURE create_order(p_customer_id NUMBER, p_product_id NUMBER);

    -- 函数：计算订单总价
    FUNCTION calculate_order_total(p_order_id NUMBER) RETURN NUMBER;

    -- 函数：查询用户购买记录
    FUNCTION get_customer_purchase_history(p_customer_id NUMBER) RETURN SYS_REFCURSOR;
END SalesPackage;
```
![程序包](pic9.png)

#### 2、创建程序包体
```
-- 创建程序包体
CREATE OR REPLACE PACKAGE BODY SalesPackage AS
    -- 存储过程：生成订单
    PROCEDURE create_order(p_customer_id NUMBER, p_product_id NUMBER) IS
        v_order_id NUMBER;
        v_quantity NUMBER;
        v_unit_price NUMBER;
    BEGIN
        -- 在Order表中插入新订单
        INSERT INTO "Order" (order_id, customer_id, order_date, status)
        VALUES (order_id_seq.NEXTVAL, p_customer_id, SYSDATE, 'New')
        RETURNING order_id INTO v_order_id;

        -- 获取商品的数量和单价
        SELECT quantity, price INTO v_quantity, v_unit_price
        FROM Product
        WHERE product_id = p_product_id;

        -- 在OrderDetail表中插入订单明细
        INSERT INTO OrderDetail (order_detail_id, order_id, product_id, quantity, unit_price)
        VALUES (order_detail_id_seq.NEXTVAL, v_order_id, p_product_id, v_quantity, v_unit_price);

        -- 更新商品表的库存
        UPDATE Product
        SET stock = stock - v_quantity
        WHERE product_id = p_product_id;
    END;

    -- 函数：计算订单总价
    FUNCTION calculate_order_total(p_order_id NUMBER) RETURN NUMBER IS
        v_total_price NUMBER;
    BEGIN
        SELECT SUM(quantity * unit_price) INTO v_total_price
        FROM OrderDetail
        WHERE order_id = p_order_id;

        RETURN v_total_price;
    END;

    -- 函数：查询用户购买记录
    FUNCTION get_customer_purchase_history(p_customer_id NUMBER) RETURN SYS_REFCURSOR IS
        v_cursor SYS_REFCURSOR;
    BEGIN
        OPEN v_cursor FOR
        SELECT od.order_id, p.name, od.quantity, od.unit_price
        FROM OrderDetail od
        INNER JOIN Product p ON od.product_id = p.product_id
        WHERE EXISTS (
            SELECT 1
            FROM "Order" o
            WHERE o.order_id = od.order_id
            AND o.customer_id = p_customer_id
        );

        RETURN v_cursor;
    END;
END SalesPackage;
```
![程序包](pic14.png)

#### 3、计算订单总价

```
-- 函数：计算订单总价
  FUNCTION calculate_order_total(p_order_id NUMBER) RETURN NUMBER IS
    v_total_price NUMBER;
  BEGIN
    SELECT SUM(quantity * unit_price) INTO v_total_price
    FROM OrderDetail
    WHERE order_id = p_order_id;
      RETURN v_total_price;
  END;
```
![订单总价](pic19.png)  
![订单总价](pic20.png)

### 6、备份方案

以下是一个基本的数据库备份方案，使用Oracle的备份工具（如RMAN）进行备份操作：

1. 配置RMAN环境：确保在Oracle数据库服务器上正确配置和安装了RMAN备份工具，并确保具有执行备份和恢复操作所需的权限。

2. 确定备份策略：根据业务需求和可接受的恢复点目标（Recovery Point Objective，RPO）和恢复时间目标（Recovery Time Objective，RTO），确定备份频率和备份类型。一般建议采用定期的完整备份和增量备份策略。

3. 定期进行完整备份：执行完整备份操作，将整个数据库的数据和日志文件备份到安全的存储位置。完整备份通常在整个数据库的基础上创建一个初始备份集，之后的备份将基于该备份集进行增量备份。

4. 执行增量备份：根据备份策略，执行增量备份操作，只备份发生更改的数据块和日志文件。增量备份通常包括增量备份级别0（完全备份）和增量备份级别1（仅备份自上次备份以来的更改）。

5. 存储备份数据：将备份数据存储在安全的位置，如磁盘阵列、磁带库或云存储中。确保备份数据可以在需要时进行快速恢复。

6. 定期测试备份的可用性：定期测试备份的可用性和完整性，恢复数据库以验证备份策略的有效性。确保备份数据能够成功恢复，并进行必要的修复和调整。

7. 日志和监控：记录备份操作的日志，并建立监控机制以确保备份任务按计划执行，并及时处理任何备份失败或异常情况。

但是，备份策略需要根据实际需求进行调整和优化。建议与数据库管理员和系统管理员合作，确保备份操作的有效性和数据的安全性。另外，根据业务需求和合规要求，可能需要采取其他安全措施，如加密备份数据、保留多个备份副本等。

## 总结
  
在该项目中，我成功地设计和实施了基于Oracle数据库的商品销售系统。通过合理的表结构设计、权限分配和存储过程开发，实现了系统的核心功能和复杂业务逻辑。数据库备份方案的设计保证了数据的安全性和可恢复性，提高了系统的稳定性和可靠性。通过这个项目的完成，加强了我对Oracle数据库的理解和应用能力，提升了数据库设计和开发的技能。同时，也为未来类似项目的开展提供了宝贵的经验和教训。
